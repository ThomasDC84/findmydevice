package de.nulide.findmydevice.net.interfaces;

public interface PostListener {

    public void onRestFinished(boolean success);
}
